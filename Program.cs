using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using System.Text;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication;

var builder = WebApplication.CreateBuilder(args);

// Add CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "AllPolicy", policy =>
    {
        policy.AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod();
    });
});

// Add Kestrel
builder.WebHost.ConfigureKestrel(serverOprions =>
{
    serverOprions.Limits.MaxRequestBodySize = 100 * 1024 * 1024;
    serverOprions.Limits.MaxConcurrentConnections = 100;
    serverOprions.Limits.RequestHeadersTimeout = TimeSpan.FromSeconds(50);
});

//JWT
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("JWTKey").Value)),
        ValidateIssuer = false,
        ValidateAudience =false,
        ClockSkew = TimeSpan.Zero

    };
});

// Add DBContext
var ConnectionString = builder.Configuration.GetConnectionString("APIContext");

builder.Services.AddDbContext<APIContext>(options =>
    options.UseSqlServer(ConnectionString));

//builder.Services.AddDbContext<IdentityContext>(options =>
//    options.UseSqlServer(IdentityConnectionString));

//builder.Services.AddDefaultIdentity<Users>(options => options.SignIn.RequireConfirmedAccount = true)
//    .AddRoles<IdentityRole>()
//    .AddEntityFrameworkStores<IdentityContext>();


// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
    {
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
        Name = "Authorization",
        Description = "Bearer Authorization with JWT Token",
        Type = Microsoft.OpenApi.Models.SecuritySchemeType.Http
    });
    options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Id = "Bearer",
                    Type = ReferenceType.SecurityScheme
                }
            },
            new List<string>()
        }
    });
});



var app = builder.Build();

// Global CORS Policy
//app.UseCors(options =>
//{
//    options.AllowAnyOrigin();
//    options.AllowAnyHeader();
//    options.AllowAnyMethod();
//});


var IsShowSwagger = builder.Configuration.GetValue<bool>("IsShowSwagger");


// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

if (IsShowSwagger)
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapGet("/jwt-token/context",async (HttpContext ctx) =>
{
    string token = await ctx.GetTokenAsync("jwt");
    return Results.Ok(new {token = token});
});

app.MapGet("/", () => "API is Running");
app.UseCors("AllPolicy");

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
