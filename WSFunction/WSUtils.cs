﻿using Microsoft.EntityFrameworkCore;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using System.Net;
using System.Security.Policy;
using System.Text;
using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;

namespace WSLawSurvey.WSFunction
{
    public static class WSUtils
    {
        private static readonly IConfiguration? _configuration;
        private static readonly IntraLoginRes _wsRes = new IntraLoginRes();

        public static async Task<bool> CheckToken(string? token, APIContext _context)
        {
            var checkToken = await _context.Users.Where(t => t.Token == token).ToListAsync();
            if (checkToken.Count == 0)
            {
                return false;
            }

            var userData = checkToken.FirstOrDefault();

            DateTime tokenExp = Convert.ToDateTime(userData.TokenExpire);
            TimeSpan dateDiff = tokenExp - DateTime.Now;

            if (dateDiff.Minutes < 0)
            {
                return false;
            }
            userData.TokenExpire = DateTime.UtcNow.AddDays(1);
            _context.Entry(userData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static async Task<IntraLoginRes> IntraSignIn(IntraSignInObjReq signInObjReq)
        {
            if (signInObjReq == null)
            {
                _wsRes.result = "false";
                _wsRes.description = "ไม่พบข้อมูลที่ส่งมา";
                _wsRes.rows = 0;
                return _wsRes;
            }

            string IntraAPI = "https://intra.tot.co.th/api/SignIn.ashx";
            //var jsonData = JsonConvert.SerializeObject(signInObjReq, Formatting.Indented);

            string? serviceResponseStr = await PostIntraData(IntraAPI+"?action=Authen", signInObjReq);

            IntraLoginRes? intraSignInObjRes = new IntraLoginRes();
            intraSignInObjRes = JsonConvert.DeserializeObject<IntraLoginRes>(serviceResponseStr);

            if (intraSignInObjRes.result != "success")
            {
                _wsRes.result = "false";
                _wsRes.description = intraSignInObjRes.description;
                _wsRes.rows = 0;
                return _wsRes;
            }

            
            intraSignInObjRes.description = "เข้าสู่ระบบ NT Intra สำเร็จ";

            return intraSignInObjRes;
        }

        public static async Task<string> PostData(string apiURL, string DataJson)
        {
            string? retData = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    StringContent? contentData = null;
                    if (!string.IsNullOrEmpty(DataJson))
                    {
                        contentData = new StringContent(DataJson, Encoding.UTF8, "application/json");
                    }
                    HttpResponseMessage response = await client.PostAsync(apiURL, contentData);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        retData = data;
                    }
                    else
                    {
                        retData = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return retData;
        }

        public static async Task<string> PostIntraData(string apiURL, IntraSignInObjReq signInObjReq)
        {
            string? retData = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var formData = new MultipartFormDataContent();
                    formData.Add(new StringContent(signInObjReq.Client_key), "client_key");
                    formData.Add(new StringContent(signInObjReq.Username), "username");
                    formData.Add(new StringContent(signInObjReq.Password), "password");
                    formData.Add(new StringContent(signInObjReq.Ip), "ip");
                    formData.Headers.Add("ContentType", "multipart/form-data");

                    HttpResponseMessage response = await client.PostAsync(apiURL, formData);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        retData = data;
                    }
                    else
                    {
                        retData = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                IntraLoginRes? intraSignInObjRes = new IntraLoginRes();
                intraSignInObjRes.data = null;
                intraSignInObjRes.result = "fail";
                intraSignInObjRes.description = ex.Message;

                return JsonConvert.SerializeObject(intraSignInObjRes);
                //Console.WriteLine(ex.Message);
            }
            return retData;
        }
    }
}
