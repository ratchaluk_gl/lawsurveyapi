﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using WSLawSurvey.WSFunction;

namespace WSLawSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Controller
    {
        private readonly APIContext _context;
        private readonly WSRespons _wsRes = new WSRespons();

        public AdminController(APIContext context)
        {
            _context = context;
        }

        [HttpPost("GetAllSurvey")]
        [Authorize]
        public async Task<IActionResult> GetAllSurvey(CheckTokenReq tokenReq)
        {
            if (!await WSUtils.CheckToken(tokenReq.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var Surveys = await _context.Survey.ToListAsync();
            if (Surveys == null)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "ไม่พบรายการแบบสอบทาน";
                _wsRes.Result = null;
                return Ok(_wsRes);
            }

            _wsRes.CallAPIStatus = true;
            _wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
            _wsRes.Result = Surveys;
            return Ok(_wsRes);

        }

        [HttpPost("GetAllDepartmentProgress")]
        [Authorize]
        public async Task<IActionResult> GetAllDepartmentProgress(GetAllDepartmentReq getAllDepartmentReq)
        {
            if (!await WSUtils.CheckToken(getAllDepartmentReq.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }
            // Get Active Survey
            var activeSurvey = await _context.Survey.Where(sv => sv.IsActive == 1)
                                                    .Where(sv => sv.SurveyId == getAllDepartmentReq.SurveyId)
                                                    .Where(sv => sv.EndDate > DateTime.Now)
                                                    .FirstOrDefaultAsync();

            // Get User who can do the Active survey By Department Id
            var distinct = await _context.SurveyPermission.Where(sp => sp.IsActive == 1 && sp.SurveyId == getAllDepartmentReq.SurveyId)
                                                             .ToListAsync();

            var permitUsers = distinct.Select(sp => sp.DepartmentId).Distinct().ToList();

            var userCount = permitUsers.Count;

            var actionUser = 0;
            for (int i = 0; i < userCount; i++)
            {
                var users = await _context.Answer.Where(ans => ans.DepartmentId == permitUsers[i])
                                                 .Where(ans => ans.SurveyId == getAllDepartmentReq.SurveyId)
                                                 .FirstOrDefaultAsync();
                if (users != null)
                {
                    actionUser++;
                }
            }
            var noActionUser = userCount - actionUser;

            List<ResDepartmentListWithProcess>? resDepartmentListWithProcesses = new List<ResDepartmentListWithProcess>();

            // Get Department Percentage
            for (int i = 0; i < userCount; i++)
            {
                var depName = await _context.Department.Where(dp => dp.DepartmentId == permitUsers[i])
                                                   //.Select(dp => dp.DepartmentNameTH)
                                                   .FirstOrDefaultAsync();

                //var secName = await _context.Users.Where(us => us.UserId == permitUsers[i].UserId)
                //                                  .Select(us => us.Section)
                //                                  .FirstOrDefaultAsync();

                var percentage = await _context.UserProcess.Where(up => up.DepartmentId == permitUsers[i])
                                                           .Where(up => up.PercentageFor == 1)
                                                              //.DistinctBy(up => up.DepartmentId)
                                                              .OrderByDescending(up => up.ProcessSequence)
                                                              .ToListAsync();

                var depPercentage = percentage.FirstOrDefault();
                DateTime surveyStartDate = Convert.ToDateTime(activeSurvey.StartDate);

                //var pre = percentage.Where(pst => pst.ProcessingDate < Convert.ToDateTime("2024-03-12")).FirstOrDefault();
                var pre = percentage.Where(pst => pst.ProcessingDate < surveyStartDate.AddDays(7)).FirstOrDefault(); ;
                var post = percentage.Where(pst => pst.ProcessingDate > surveyStartDate.AddDays(14)).FirstOrDefault();

                ResDepartmentListWithProcess departmentListWithProcess = new ResDepartmentListWithProcess();

                departmentListWithProcess.DepartmentId = permitUsers[i];
                departmentListWithProcess.DepartmentName = depName.DepartmentNameTH;
                departmentListWithProcess.DepartmentInitName = depName.InitialName;
                //departmentListWithProcess.SectionName = secName;
                departmentListWithProcess.Percentage = depPercentage == null ? "17.5" : depPercentage.Percentage.ToString();
                departmentListWithProcess.Color = depPercentage == null ? 1 : depPercentage.Color;
                departmentListWithProcess.DepartmentStatusId = 0;
                departmentListWithProcess.PrePercentage = pre == null ? "17.5" : pre.Percentage.ToString();
                departmentListWithProcess.PostPercentage = post == null ? "0" : post.Percentage.ToString();

                resDepartmentListWithProcesses.Add(departmentListWithProcess);
            }

            ResAllDepartmentStat resAllDepartmentStat = new ResAllDepartmentStat();

            resAllDepartmentStat.TotalCount = userCount;
            resAllDepartmentStat.ActionAnswerCount = actionUser;
            resAllDepartmentStat.NoActionAnswerCount = noActionUser;
            resAllDepartmentStat.DepartmentList = resDepartmentListWithProcesses;

            _wsRes.CallAPIStatus = true;
            _wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
            _wsRes.Result = resAllDepartmentStat;

            return Ok(_wsRes);
        }

        [Authorize]
        [HttpPost("GetDepartmentSurvey")]
        public async Task<ActionResult> GetDepartmentSurvey(GetDepartmentSurveyReq getSurveyReq)
        {

            if (!await WSUtils.CheckToken(getSurveyReq.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var activeSurvey = await _context.Survey.Where(sv => sv.IsActive == 1)
                                                    .Where(sv => sv.SurveyId == getSurveyReq.SurveyId)
                                                    .OrderByDescending(sv => sv.SurveyId).FirstOrDefaultAsync();
            if (activeSurvey == null)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "ไม่พบข้อมูลแบบสอบทาน";
                _wsRes.Result = null;

                return Ok(_wsRes);
            }

            //var activeSurvey = surVey.FirstOrDefault();

            try
            {
                var mainQ = await _context.MainQuestion!.Where(m => m.SurveyId == activeSurvey.SurveyId).ToListAsync();

                var userId = await _context.Users.Where(u => u.Token == getSurveyReq.Token).FirstOrDefaultAsync();

                List<ResponseMainQuestion> listResponseMainQuestion = new List<ResponseMainQuestion>();

                for (int m = 0; m < mainQ.Count; m++)
                {
                    ResponseMainQuestion responseMainQuestion = new ResponseMainQuestion();

                    responseMainQuestion.SurveyId = mainQ[m].SurveyId;
                    responseMainQuestion.DepartmentId = userId.DepartmentId;
                    responseMainQuestion.QuestionId = mainQ[m].QuestionId;
                    responseMainQuestion.QuestionNo = mainQ[m].QuestionNo.ToString();
                    responseMainQuestion.QuestionText = mainQ[m].QuestionText;

                    string? retMainProcess = "17.5";

                    responseMainQuestion.MainProcessPercentage = Double.Parse(retMainProcess);

                    // Get sub question by main question id 
                    var subQ = await _context.SubQuestion.Where(sq => sq.MainQuestionId == mainQ[m].QuestionId).ToListAsync();


                    List<ResponseSubQuestion> listResponseSubQuestion = new List<ResponseSubQuestion>();

                    double percentCount = 0;
                    int mainColor = 0;
                    for (int s = 0; s < subQ.Count; s++)
                    {
                        ResponseSubQuestion responseSubQuestion = new ResponseSubQuestion();

                        responseSubQuestion.SubQuestionId = subQ[s].SubQuestionId;
                        responseSubQuestion.SubQuestionNo = subQ[s].SubQuestionNo;
                        responseSubQuestion.SubQuestionText = subQ[s].SubQuestionText;

                        // Get answer
                        var subQanswer = await _context.Answer.Where(sqa => sqa.SubQuestionId == subQ[s].SubQuestionId)
                                                              .Where(sqa => sqa.DepartmentId == userId.DepartmentId)
                                                              .OrderByDescending(sqa => sqa.AnswerId)
                                                              .FirstOrDefaultAsync();

                        var choice = 0;
                        var answerStatus = 0;
                        string answerOtherText = "";
                        string answerStatusText = "";
                        //if (!String.IsNullOrEmpty(subQanswer.ChooseId.ToString()))
                        if (subQanswer != null)
                        {
                            choice = subQanswer.ChoiceNo;
                            answerStatus = subQanswer.AnswerStatus;
                            answerStatusText = subQanswer.AnswerStatusText;
                            if (choice == 4)
                            {
                                answerOtherText = subQanswer.OtherText;
                            }
                        }

                        responseSubQuestion.ChoiceNo = choice;
                        responseSubQuestion.OtherText = answerOtherText;

                        // Get Percentage => PercentageFor == 3 it's mean SubQuestion
                        var subPercentage = await _context.UserProcess.Where(spt => spt.PercentageFor == 3)
                                                                      .Where(spt => spt.PercentageForId == subQ[s].SubQuestionId)
                                                                      .Where(spt => spt.DepartmentId == userId.DepartmentId).FirstOrDefaultAsync();

                        string? percentage = "17.5";
                        int Color = 1;

                        if (subPercentage != null)
                        {
                            percentage = subPercentage.Percentage.ToString();
                            Color = subPercentage.Color;
                        }

                        var AdminMemo = await _context.SubQuestionMemo.Where(am => am.SurveyId == mainQ[m].SurveyId)
                                                                      .Where(am => am.MainQuestionId == subQ[s].MainQuestionId)
                                                                      .Where(am => am.SubQuestionId == subQ[s].SubQuestionId)
                                                                      .OrderByDescending(am => am.CommentSequence)
                                                                      .Select(am => am.CommentText)
                                                                      .FirstOrDefaultAsync();
                        if (AdminMemo == null)
                        {
                            AdminMemo = "";
                        }

                        responseSubQuestion.CommentFromAdmin = AdminMemo.ToString();
                        responseSubQuestion.SubProcessPercentage = Double.Parse(percentage);
                        responseSubQuestion.SubQuestionStatus = answerStatus;
                        responseSubQuestion.SubQuestionStatusText = answerStatusText;
                        responseSubQuestion.SubQuestionColor = Color;

                        listResponseSubQuestion.Add(responseSubQuestion);

                        // main color
                        if (Double.Parse(percentage) < 43)
                        {
                            mainColor = 1;
                        }
                        else if (Double.Parse(percentage) > 43 && Double.Parse(percentage) < 60.5)
                        {
                            mainColor = 2;
                        }

                        percentCount = percentCount + Double.Parse(percentage);
                    }
                    responseMainQuestion.SubQuestions = listResponseSubQuestion;

                    retMainProcess = (percentCount / subQ.Count).ToString();

                    responseMainQuestion.MainProcessPercentage = Double.Parse(retMainProcess);

                    // main color
                    if (mainColor == 0)
                    {
                        if (Double.Parse(retMainProcess) <= 35)
                        {
                            mainColor = 1; //red
                        }
                        else if (Double.Parse(retMainProcess) <= 50)
                        {
                            mainColor = 2; //orange
                        }
                        else if (Double.Parse(retMainProcess) <= 70)
                        {
                            mainColor = 3; //yellow
                        }
                        else if (Double.Parse(retMainProcess) <= 85)
                        {
                            mainColor = 4; //light green
                        }
                        else if (Double.Parse(retMainProcess) <= 100)
                        {
                            mainColor = 5; //green
                        }
                    }

                    responseMainQuestion.MainQuestionColor = mainColor;


                    listResponseMainQuestion.Add(responseMainQuestion);

                }

                _wsRes.CallAPIStatus = true;
                _wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
                _wsRes.Result = new { SurveyId = mainQ[0].SurveyId, Explanations = activeSurvey.Explanations, QuestionList = listResponseMainQuestion };

                return Ok(_wsRes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetDepartmentSubQuestionById")]
        [Authorize]
        public async Task<IActionResult> GetDepartmentSubQuestionById(GetDepartmentSubQuestionByIdReq getSubQById)
        {
            if (!await WSUtils.CheckToken(getSubQById.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == getSubQById.Token);

            var mainQ = await _context.MainQuestion.Where(mq => mq.QuestionId == getSubQById.MainQuestionId)
                                                   .FirstOrDefaultAsync();

            var subQ = await _context.SubQuestion.Where(sq => sq.MainQuestionId == getSubQById.MainQuestionId)
                                                 .Where(sq => sq.SubQuestionId == getSubQById.SubQuestionId)
                                                 .FirstOrDefaultAsync();

            if (subQ == null || mainQ == null)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "ไม่พบข้อมูลแบบสอบทาน";
                _wsRes.Result = null;

                return Ok(_wsRes);
            }

            // Get answer
            var subQanswer = await _context.Answer.Where(sqa => sqa.SubQuestionId == subQ.SubQuestionId)
                                                  .Where(sqa => sqa.DepartmentId == getSubQById.DepartmentId)
                                                  .OrderByDescending(sqa => sqa.AnswerSequence)
                                                  .FirstOrDefaultAsync();

            var choice = 0;
            var answerStatus = 0;
            var hasFile = 0;
            var ansStatusTxt = "";

            List<ResponseFiles>? resFiles = new List<ResponseFiles>();
            if (subQanswer != null)
            {
                choice = subQanswer.ChoiceNo;
                answerStatus = subQanswer.AnswerStatus;
                ansStatusTxt = subQanswer.AnswerStatusText == null ? "" : subQanswer.AnswerStatusText;

                hasFile = subQanswer.HasFile;
                // Get Files
                var files = await _context.Files.Where(f => f.DepartmentId == getSubQById.DepartmentId)
                                                .Where(f => f.MainQuesetionId == subQ.MainQuestionId)
                                                .Where(f => f.SubQuestionId == subQ.SubQuestionId)
                                                .Where(f => f.IsActive == 1).ToListAsync();

                if (files != null && files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        ResponseFiles responseFiles = new ResponseFiles();
                        responseFiles.FileId = file.Id;
                        responseFiles.FileName = file.FileName;
                        responseFiles.FileType = file.FileType;
                        responseFiles.Base64Str = file.Base64Str;

                        resFiles.Add(responseFiles);
                    }
                }

            }

            // Get Percentage
            var subPercentage = await _context.UserProcess.Where(spt => spt.PercentageFor == 3)
                                                          .Where(spt => spt.PercentageForId == subQ.SubQuestionId)
                                                          .Where(spt => spt.DepartmentId == getSubQById.DepartmentId).FirstOrDefaultAsync();

            string? percentage = "17.5";
            int Color = 1;

            if (subPercentage != null)
            {
                percentage = subPercentage.Percentage.ToString();
                Color = subPercentage.Color;
            }

            ResponseSubQuestion responseSubQuestion = new ResponseSubQuestion();

            responseSubQuestion.SubQuestionId = subQ.SubQuestionId;
            responseSubQuestion.SubQuestionNo = subQ.SubQuestionNo;
            responseSubQuestion.SubQuestionText = subQ.SubQuestionText;
            responseSubQuestion.SurveyId = mainQ.SurveyId;
            responseSubQuestion.MainQuestionId = mainQ.QuestionId;
            responseSubQuestion.MainQuestionText = mainQ.QuestionText;
            responseSubQuestion.ChoiceNo = choice;
            responseSubQuestion.OtherText = choice == 4 ? subQanswer.OtherText : "";
            responseSubQuestion.HasFiles = hasFile;
            responseSubQuestion.Files = resFiles;
            responseSubQuestion.SubProcessPercentage = Double.Parse(percentage);
            responseSubQuestion.SubQuestionStatus = answerStatus;
            responseSubQuestion.SubQuestionStatusText = ansStatusTxt;
            responseSubQuestion.SubQuestionColor = Color;

            _wsRes.CallAPIStatus = true;
            _wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
            _wsRes.Result = responseSubQuestion;

            return Ok(_wsRes);
        }

        [HttpPost("AddNewSurvey")]
        [Authorize]
        public async Task<IActionResult> AddNewSurvey(AddNewSurveyReq addNewSurveyReq)
        {
            if (!await WSUtils.CheckToken(addNewSurveyReq.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == addNewSurveyReq.Token);

            Survey survey = new Survey();

            survey.SurveyName = addNewSurveyReq.SurveyName;
            survey.SurveyDetail = addNewSurveyReq.SurveyDetail;
            survey.Explanations = addNewSurveyReq.Explanations;
            survey.StartDate = addNewSurveyReq.StartDate;
            survey.EndDate = addNewSurveyReq.EndDate;
            survey.IsActive = 1;
            survey.CreatedBy = userId.UserId;
            survey.CreatedDate = DateTime.Now;

            await _context.AddAsync(survey);

            try
            {
                await _context.SaveChangesAsync();

                _wsRes.CallAPIStatus = true;
                _wsRes.CallAPIStatusText = "เพิ่มแบบสอบทานสำเร็จ";
                _wsRes.Result = survey;

                return Ok(_wsRes);
            }
            catch (Exception ex)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "เพิ่มแบบสอบทานไม่สำเร็จ";
                _wsRes.Result = new {error = ex.ToString()};

                return Ok(_wsRes);
            }

            
        }

        [HttpPost("AddMainQuestion")]
        [Authorize]
        public async Task<IActionResult> AddMainQuestion(AddMainQuestionReq addMainQuestion)
        {
            if (!await WSUtils.CheckToken(addMainQuestion.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            return Ok(_wsRes);
        }

        [HttpPost("RespondentsAssignment")]
        [Authorize]
        public async Task<IActionResult> RespondentsAssignment(ResponsdentsAssignmentReq responsdentsAssignmentReq)
        {
            if (!await WSUtils.CheckToken(responsdentsAssignmentReq.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var user = await _context.Users.Where(u => u.UserName == responsdentsAssignmentReq.UserName)
                                            .Where(u => u.IsActive == 1)
                                            .OrderByDescending(u => u.UserId)
                                            .FirstOrDefaultAsync();


            if (user == null)
            {
                //Add New User to User DB
                Users users = new Users();
                users.UserName = responsdentsAssignmentReq.UserName;
                users.FirstName = responsdentsAssignmentReq.FirstName;
                users.LastName = responsdentsAssignmentReq.LastName;
                users.DepartmentId = responsdentsAssignmentReq.DepartmentId;
                users.PhoneNo = responsdentsAssignmentReq.PhoneNo;
                users.UserTypeId = responsdentsAssignmentReq.UserTypeId;

                await _context.Users.AddAsync(users);
               
            }
            else
            {
                user.FirstName = responsdentsAssignmentReq.FirstName;
                user.LastName = responsdentsAssignmentReq.LastName;
                user.DepartmentId = responsdentsAssignmentReq.DepartmentId;
                user.PhoneNo = responsdentsAssignmentReq.PhoneNo;
                user.UserTypeId = responsdentsAssignmentReq.UserTypeId;

                _context.Entry(user).State = EntityState.Modified;
            }
            try
            {
                await _context.SaveChangesAsync();

                var assignUser = await _context.Users.Where(u => u.UserName == responsdentsAssignmentReq.UserName)
                                                     .FirstOrDefaultAsync();

                SurveyPermission permission = new SurveyPermission();
                permission.SurveyId = (int)responsdentsAssignmentReq.SurveyId;
                permission.UserId = (int)assignUser.UserId;
                permission.DepartmentId = responsdentsAssignmentReq.DepartmentId;
                permission.IsActive = 1;
                permission.CreatedDate = DateTime.Now;

                await _context.SurveyPermission.AddAsync(permission);
                await _context.SaveChangesAsync();

                _wsRes.CallAPIStatus = true;
                _wsRes.CallAPIStatusText = "เพิ่มผู้ตอบแบบสอบทานสำเร็จ";
                _wsRes.Result = responsdentsAssignmentReq;

                return Ok(_wsRes);
            }
            catch (Exception ex)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "เพิ่มผู้ตอบแบบสอบทานไม่สำเร็จ";
                _wsRes.Result = new {error = ex.ToString()};

                return Ok(_wsRes);
            }
            
        }

        [HttpPost("AddCommentTextToSubQuestion")]
        [Authorize]
        public async Task<IActionResult> AddCommentTextToSubQuestion(AddCommentToSubQuestionReq addComment)
        {
            if (!await WSUtils.CheckToken(addComment.Token, _context))
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "Token หมดอายุ";
                _wsRes.Result = null;

                return Unauthorized(_wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == addComment.Token);

            var subQmemo = await _context.SubQuestionMemo.Where(sqm => sqm.SurveyId == addComment.SurveyId)
                                                         .Where(sqm => sqm.MainQuestionId == addComment.MainQuestionId)
                                                         .Where(sqm => sqm.SubQuestionId == addComment.SubQuestionId)
                                                         .OrderByDescending(sqm => sqm.CommentSequence)
                                                         .ToListAsync();

            var memoSeq = 1;
            if (subQmemo != null)
            {
                memoSeq = (int)(subQmemo.Select(sqm => sqm.CommentSequence).FirstOrDefault()) + 1;
            }
             
            SubQuestionMemo subQuestionMemo = new SubQuestionMemo();
            subQuestionMemo.SurveyId = addComment.SurveyId;
            subQuestionMemo.MainQuestionId = addComment.MainQuestionId;
            subQuestionMemo.SubQuestionId = addComment.SubQuestionId;
            subQuestionMemo.CommentText = addComment.CommentText;
            subQuestionMemo.CommentSequence = memoSeq;
            subQuestionMemo.CommentDate = DateTime.Now;
            subQuestionMemo.CommentBy = userId.UserId.ToString();

            await _context.SubQuestionMemo.AddAsync(subQuestionMemo);

            try
            {
                await _context.SaveChangesAsync();

                _wsRes.CallAPIStatus = true;
                _wsRes.CallAPIStatusText = "เพิ่มข้อความสำเร็จ";
                _wsRes.Result = subQuestionMemo;

                return Ok(_wsRes);
            }
            catch(Exception ex)
            {
                _wsRes.CallAPIStatus = false;
                _wsRes.CallAPIStatusText = "เพิ่มข้อความไม่สำเร็จ";
                _wsRes.Result = new {error = ex.ToString(),data = subQuestionMemo};

                return Ok(_wsRes);
            }

        }
    }
}
