﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using WSLawSurvey.WSFunction;

namespace WSLawSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveysController : ControllerBase
    {
        private readonly APIContext _context;
        private readonly WSRespons wsRes = new WSRespons();

        public SurveysController(APIContext context)
        {
            _context = context;
            //_baseController = baseController;
        }
        [Authorize]
        [HttpPost("GetUsersSurveyList")]
        public async Task<IActionResult> GetUsersSurveyList(CheckTokenReq tokenReq)
        {
            if (!await WSUtils.CheckToken(tokenReq.Token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }
            var userId = await _context.Users.Where(u => u.Token == tokenReq.Token).FirstOrDefaultAsync();

            //var hasSurvey = await _context.SurveyPermission.Where(hs => hs.DepartmentId == userId.DepartmentId).ToListAsync();
            //.DistinctBy(hs => hs.SurveyId)

            var hasSurvey = await _context.SurveyPermission.Select(hs => new {hs.SurveyId , hs.DepartmentId})
                                                           .Where(hs => hs.DepartmentId == userId.DepartmentId).Distinct().ToListAsync();

            if (hasSurvey == null)
            {
                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "ไม่พบแบบสอบทานสำหรับท่าน";

                return Ok(wsRes);
            }

            List<ResponseUsersSurvey>? responseUsersSurveys = new List<ResponseUsersSurvey>();
            // loop for get survey name
            foreach (var sv in hasSurvey)
            {
                ResponseUsersSurvey responseUsersSurvey = new ResponseUsersSurvey();

                var surveyInfo = await _context.Survey.Where(si => si.SurveyId == sv.SurveyId).FirstOrDefaultAsync();
                if (surveyInfo == null)
                {
                    wsRes.CallAPIStatus = true;
                    wsRes.CallAPIStatusText = "ไม่พบแบบสอบทานในระบบ";
                    return Ok(wsRes);
                }

                responseUsersSurvey.SurveyId = surveyInfo.SurveyId;
                responseUsersSurvey.SurveyName = surveyInfo.SurveyName;
                responseUsersSurvey.SurveyDetail = surveyInfo.SurveyDetail;
                responseUsersSurvey.StartDate = surveyInfo.StartDate;
                responseUsersSurvey.EndDate = surveyInfo.EndDate;
                responseUsersSurvey.IsActive = surveyInfo.IsActive;

                responseUsersSurveys.Add(responseUsersSurvey);
            }

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
            wsRes.Result = responseUsersSurveys;

            return Ok(wsRes);
        }

        // GET: api/Surveys
        [Authorize]
        [HttpPost("GetSurvey")]
        public async Task<ActionResult> GetSurvey(GetSurveyReq getSurveyReq)
        {

            if (!await WSUtils.CheckToken(getSurveyReq.Token,_context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }

            var activeSurvey = await _context.Survey.Where(sv => sv.IsActive == 1)
                                                    .Where(sv => sv.SurveyId == getSurveyReq.SurveyId)
                                                    .OrderByDescending(sv => sv.SurveyId).FirstOrDefaultAsync();
            if(activeSurvey == null)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ไม่พบข้อมูลแบบสอบทาน";
                wsRes.Result = null;

                return Ok(wsRes);
            }

            //var activeSurvey = surVey.FirstOrDefault();

            try
            {
                var mainQ = await _context.MainQuestion!.Where(m => m.SurveyId == activeSurvey.SurveyId).ToListAsync();

                if (mainQ.Count == 0)
                {
                    wsRes.CallAPIStatus = false;
                    wsRes.CallAPIStatusText = "ไม่พบคำถามหลักสำหรับแบบสอบทาน " + activeSurvey.SurveyName;

                    return Ok(wsRes);
                }

                var userId = await _context.Users.Where(u => u.Token == getSurveyReq.Token).FirstOrDefaultAsync();

                List<ResponseMainQuestion> listResponseMainQuestion = new List<ResponseMainQuestion>();

                for (int m = 0; m < mainQ.Count; m++)
                {
                    ResponseMainQuestion responseMainQuestion = new ResponseMainQuestion();

                    responseMainQuestion.SurveyId = mainQ[m].SurveyId;
                    responseMainQuestion.UserId = userId.UserId;
                    responseMainQuestion.QuestionId = mainQ[m].QuestionId;
                    responseMainQuestion.QuestionNo = mainQ[m].QuestionNo.ToString();
                    responseMainQuestion.QuestionText = mainQ[m].QuestionText;

                    // Get Percentage from tb user process => PercentageFor == 2 it's mean MainQuestion
                    //var mainProcess = await _context.UserProcess!.Where(mp => mp.PercentageFor == 2)
                    //                                              .Where(mp => mp.PercentageForId == mainQ[m].QuestionId)
                    //                                              .Where(mp => mp.UserId == userId.UserId)
                    //                                              .FirstOrDefaultAsync();

                    string? retMainProcess = "17.5";
                    //if (mainProcess != null)
                    //{
                    //    retMainProcess = mainProcess.Percentage.ToString();
                    //}



                    responseMainQuestion.MainProcessPercentage = Double.Parse(retMainProcess);

                    // Get sub question by main question id 
                    var subQ = await _context.SubQuestion.Where(sq => sq.MainQuestionId == mainQ[m].QuestionId).ToListAsync();


                    List<ResponseSubQuestion> listResponseSubQuestion = new List<ResponseSubQuestion>();

                    double percentCount = 0;
                    int mainColor = 0;
                    for (int s = 0; s < subQ.Count; s++)
                    {
                        ResponseSubQuestion responseSubQuestion = new ResponseSubQuestion();

                        responseSubQuestion.SubQuestionId = subQ[s].SubQuestionId;
                        responseSubQuestion.SubQuestionNo = subQ[s].SubQuestionNo;
                        responseSubQuestion.SubQuestionText = subQ[s].SubQuestionText;

                        // Get answer
                        var subQanswer = await _context.Answer.Where(sqa => sqa.SubQuestionId == subQ[s].SubQuestionId)
                                                              .Where(sqa => sqa.DepartmentId == userId.DepartmentId)
                                                              .OrderByDescending(sqa => sqa.AnswerId)
                                                              .FirstOrDefaultAsync();

                        var choice = 0;
                        var answerStatus = 0;
                        string answerOtherText = "";
                        string answerStatusText = "";
                        //if (!String.IsNullOrEmpty(subQanswer.ChooseId.ToString()))
                        if (subQanswer != null)
                        {
                            choice = subQanswer.ChoiceNo;
                            answerStatus = subQanswer.AnswerStatus;
                            answerStatusText = subQanswer.AnswerStatusText;
                            if (choice == 4)
                            {
                                answerOtherText = subQanswer.OtherText;
                            }
                        }

                        //responseSubQuestion.Choice1 = choice == 1 ? 1 : 0;
                        //responseSubQuestion.Choice2 = choice == 2 ? 1 : 0;
                        //responseSubQuestion.Choice3 = choice == 3 ? 1 : 0;
                        responseSubQuestion.ChoiceNo = choice;
                        responseSubQuestion.OtherText = answerOtherText;

                        // Get Percentage => PercentageFor == 3 it's mean SubQuestion
                        var subPercentage = await _context.UserProcess.Where(spt => spt.PercentageFor == 3)
                                                                      .Where(spt => spt.PercentageForId == subQ[s].SubQuestionId)
                                                                      .Where(spt => spt.DepartmentId == userId.DepartmentId)
                                                                      .OrderByDescending(spt => spt.ProcessSequence)
                                                                      .FirstOrDefaultAsync();

                        string? percentage = "17.5";
                        int Color = 1;

                        if (subPercentage != null)
                        {
                            percentage = subPercentage.Percentage.ToString();
                            Color = subPercentage.Color;
                        }

                        var AdminMemo = await _context.SubQuestionMemo.Where(am => am.SurveyId == mainQ[m].SurveyId)
                                                                      .Where(am => am.MainQuestionId == subQ[s].MainQuestionId)
                                                                      .Where(am => am.SubQuestionId == subQ[s].SubQuestionId)
                                                                      .OrderByDescending(am => am.CommentSequence)
                                                                      .Select(am => am.CommentText)
                                                                      .FirstOrDefaultAsync();
                        if (AdminMemo == null)
                        {
                            AdminMemo = "";
                        }

                        responseSubQuestion.CommentFromAdmin = AdminMemo.ToString();
                        responseSubQuestion.SubProcessPercentage = Double.Parse(percentage);
                        responseSubQuestion.SubQuestionStatus = answerStatus;
                        responseSubQuestion.SubQuestionStatusText = answerStatusText;
                        responseSubQuestion.SubQuestionColor = Color;

                        listResponseSubQuestion.Add(responseSubQuestion);

                        // main color
                        if (Double.Parse(percentage) < 43)
                        {
                            mainColor = 1;
                        }
                        else if (Double.Parse(percentage) > 43 && Double.Parse(percentage) < 60.5)
                        {
                            mainColor = 2;
                        }

                        percentCount = percentCount + Double.Parse(percentage);
                    }
                    responseMainQuestion.SubQuestions = listResponseSubQuestion;

                    retMainProcess = (percentCount/subQ.Count).ToString();

                    responseMainQuestion.MainProcessPercentage = Double.Parse(retMainProcess);

                    // main color
                    if (mainColor == 0)
                    {
                        if (Double.Parse(retMainProcess) <= 35)
                        {
                            mainColor = 1; //red
                        }
                        else if (Double.Parse(retMainProcess) <= 50)
                        {
                            mainColor = 2; //orange
                        }
                        else if (Double.Parse(retMainProcess) <= 70)
                        {
                            mainColor = 3; //yellow
                        }
                        else if (Double.Parse(retMainProcess) <= 85)
                        {
                            mainColor = 4; //light green
                        }
                        else if (Double.Parse(retMainProcess) <= 100)
                        {
                            mainColor = 5; //green
                        }
                    }

                    responseMainQuestion.MainQuestionColor = mainColor;


                    listResponseMainQuestion.Add(responseMainQuestion);

                }

                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
                wsRes.Result = new {SurveyId = mainQ[0].SurveyId, Explanations = activeSurvey.Explanations, QuestionList = listResponseMainQuestion };

                return Ok(wsRes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetSubQuestionById")]
        [Authorize]
        public async Task<IActionResult> GetSubQuestionById(GetSubQuestionByIdReq getSubQById)
        {
            if (!await WSUtils.CheckToken(getSubQById.Token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == getSubQById.Token);

            var mainQ = await _context.MainQuestion.Where(mq => mq.QuestionId == getSubQById.MainQuestionId)
                                                   .FirstOrDefaultAsync();

            var subQ = await _context.SubQuestion.Where(sq => sq.MainQuestionId == getSubQById.MainQuestionId)
                                                 .Where(sq => sq.SubQuestionId == getSubQById.SubQuestionId)
                                                 .FirstOrDefaultAsync();

            if (subQ == null || mainQ == null)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ไม่พบข้อมูลแบบสอบทาน";
                wsRes.Result = null;

                return Ok(wsRes);
            }

            // Get answer
            var subQanswer = await _context.Answer.Where(sqa => sqa.SubQuestionId == subQ.SubQuestionId)
                                                  .Where(sqa => sqa.UserId == userId.UserId)
                                                  .OrderByDescending(sqa => sqa.AnswerSequence)
                                                  .FirstOrDefaultAsync();

            var choice = 0;
            var answerStatus = 0;
            var hasFile = 0;
            var ansStatusTxt = "";
            //if (!String.IsNullOrEmpty(subQanswer.ChooseId.ToString()))
            List<ResponseFiles>? resFiles = new List<ResponseFiles>();
            if (subQanswer != null)
            {
                choice = subQanswer.ChoiceNo;
                answerStatus = subQanswer.AnswerStatus;
                ansStatusTxt = subQanswer.AnswerStatusText == null ? "": subQanswer.AnswerStatusText ;

                hasFile = subQanswer.HasFile;
                // Get Files
                var files = await _context.Files.Where(f => f.UserId == userId.UserId)
                                                .Where(f => f.MainQuesetionId == subQ.MainQuestionId)
                                                .Where(f => f.SubQuestionId == subQ.SubQuestionId)
                                                .Where(f => f.IsActive == 1).ToListAsync();

                if (files != null && files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        ResponseFiles responseFiles = new ResponseFiles();
                        responseFiles.FileId = file.Id;
                        responseFiles.FileName = file.FileName;
                        responseFiles.FileType = file.FileType;
                        responseFiles.Base64Str = file.Base64Str;

                        resFiles.Add(responseFiles);
                    }
                }

            }

            // Admin Comment
            var AdminMemo = await _context.SubQuestionMemo.Where(am => am.MainQuestionId == getSubQById.MainQuestionId)
                                                                      .Where(am => am.SubQuestionId == getSubQById.SubQuestionId)
                                                                      .OrderByDescending(am => am.CommentSequence)
                                                                      .Select(am => am.CommentText)
                                                                      .FirstOrDefaultAsync();
            if (AdminMemo == null)
            {
                AdminMemo = "";
            }

            // Get Percentage
            var subPercentage = await _context.UserProcess.Where(spt => spt.PercentageFor == 3)
                                                          .Where(spt => spt.PercentageForId == subQ.SubQuestionId)
                                                          .Where(spt => spt.UserId == userId.UserId).FirstOrDefaultAsync();

            string? percentage = "17.5";
            int Color = 1;

            if (subPercentage != null)
            {
                percentage = subPercentage.Percentage.ToString();
                Color = subPercentage.Color;
            }

            ResponseSubQuestion responseSubQuestion = new ResponseSubQuestion();

            responseSubQuestion.SubQuestionId = subQ.SubQuestionId;
            responseSubQuestion.SubQuestionNo = subQ.SubQuestionNo;
            responseSubQuestion.SubQuestionText = subQ.SubQuestionText;
            responseSubQuestion.SurveyId = mainQ.SurveyId;
            responseSubQuestion.MainQuestionId = mainQ.QuestionId;
            responseSubQuestion.MainQuestionText = mainQ.QuestionText;
            //responseSubQuestion.Choice1 = choice == 1 ? 1 : 0;
            //responseSubQuestion.Choice2 = choice == 2 ? 1 : 0;
            //responseSubQuestion.Choice3 = choice == 3 ? 1 : 0;
            responseSubQuestion.ChoiceNo = choice;
            responseSubQuestion.OtherText = choice == 4 ? subQanswer.OtherText : "";
            responseSubQuestion.CommentFromAdmin = AdminMemo.ToString();
            responseSubQuestion.HasFiles = hasFile;
            responseSubQuestion.Files = resFiles;
            responseSubQuestion.SubProcessPercentage = Double.Parse(percentage);
            responseSubQuestion.SubQuestionStatus = answerStatus;
            responseSubQuestion.SubQuestionStatusText = ansStatusTxt;
            responseSubQuestion.SubQuestionColor = Color;

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "เรียกดูข้อมูลสำเร็จ";
            wsRes.Result = responseSubQuestion;

            return Ok(wsRes);
        }

        [HttpPost("GetSubQuestionProcessHistory")]
        [Authorize]
        public async Task<IActionResult> GetSubQuestionProcessHistory(SubQuestionProcessHistoryReq processHistoryReq)
        {
            if (!await WSUtils.CheckToken(processHistoryReq.Token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == processHistoryReq.Token);

            var history = await _context.Answer.Where(his => his.UserId == userId.UserId)
                                         .Where(his => his.SubQuestionId == processHistoryReq.SubQuestionId)
                                         .Where(his => his.AnswerStatus == 1)
                                         .OrderByDescending(his => his.AnswerSequence)
                                         .ToListAsync();
            if (history.Count == 0)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ไม่พบข้อมูลประวัติ";
                wsRes.Result = null;

                return Ok(wsRes);
            }

            List<ResSubQuestionProcessHistory>? resList = new List<ResSubQuestionProcessHistory>();

            for (int hs = 0; hs < history.Count; hs++)
            {
                ResSubQuestionProcessHistory resHis = new ResSubQuestionProcessHistory();
                resHis.UserId = history[hs].UserId;
                resHis.SurveyId = history[hs].SurveyId;
                resHis.MainQuestionId = history[hs].MainQuestionId;
                resHis.SubQuestionId = history[hs].SubQuestionId;
                resHis.AnswerDate = history[hs].AnswerDate;
                resHis.Sequence = history[hs].AnswerSequence;

                var comment = await _context.SubQuestionMemo.Where(sqh => sqh.UserId == history[hs].UserId)
                                                            .Where(sqh => sqh.MainQuestionId == history[hs].MainQuestionId)
                                                            .Where(sqh => sqh.SubQuestionId == history[hs].SubQuestionId)
                                                            .Where(sqh => sqh.CommentSequence == history[hs].AnswerSequence)
                                                            .FirstOrDefaultAsync();
                var retComment = "";
                DateTime? retCommentDate = null;
                if (comment != null)
                {
                    retComment = comment.CommentText;
                    retCommentDate = comment.CommentDate;
                }

                resHis.CommentText = retComment;
                resHis.CommentDate = retCommentDate;

                var process = await _context.UserProcess.Where(ps => ps.UserId == history[hs].UserId)
                                                        .Where(ps => ps.PercentageFor == 3)
                                                        .Where(ps => ps.PercentageForId == history[hs].SubQuestionId)
                                                        .Where(ps => ps.ProcessSequence == history[hs].AnswerSequence)
                                                        .FirstOrDefaultAsync();

                string? retProcess = "17.5";
                if (process != null)
                {
                    retProcess = process.Percentage.ToString();
                }

                resHis.Percentage = retProcess;

                resList.Add(resHis);
            }

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "เรียกดูข้อมูลประวัติสำเร็จ";
            wsRes.Result = resList;

            return Ok(wsRes);
        }
        
        [Authorize]
        [HttpPost("SubmitQuestion")]
        public async Task<IActionResult> SubmitQuestion(SubmitQuestionReq submitReq)
        {
            if (!await WSUtils.CheckToken(submitReq.Token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }

            var userId = await _context.Users.FirstOrDefaultAsync(u => u.Token == submitReq.Token);
            // Check Answer Exist
            var checkAns = await _context.Answer.Where(ca => ca.DepartmentId == userId.DepartmentId)
                                                .Where(ca => ca.SurveyId == submitReq.SurveyId)
                                                .Where(ca => ca.MainQuestionId == submitReq.MainQuestionId)
                                                .Where(ca => ca.SubQuestionId == submitReq.SubQuestionId)
                                                .ToListAsync();
            Answer answer = new Answer();
            var answSeq = 1;
            var hasFile = 0;
            
            if(checkAns.Count != 0)
            {
                answSeq = checkAns[checkAns.Count - 1].AnswerSequence + 1;
            }

            //if (submitReq.Files != null)
            //{
            //    hasFile = 1;
            //}

            answer.UserId = (int)userId.UserId;
            answer.DepartmentId = userId.DepartmentId;
            answer.SurveyId = submitReq.SurveyId;
            answer.MainQuestionId = submitReq.MainQuestionId;
            answer.SubQuestionId = submitReq.SubQuestionId;
            answer.ChoiceNo = submitReq.ChoiceNo;
            answer.OtherText = submitReq.OtherText;
            answer.HasFile = hasFile;
            answer.AnswerDate = DateTime.Now;
            answer.AnswerSequence = answSeq;
            answer.AnswerStatus = 1;

            await _context.Answer.AddAsync(answer);
            await _context.SaveChangesAsync();

            // Save File
            //if (hasFile > 0)
            //{
            //    bool isSave = await SaveFile((int)userId.UserId, submitReq.MainQuestionId , submitReq.SubQuestionId , submitReq.Files);
            //    if (!isSave)
            //    {
            //        wsRes.CallAPIStatus = true;
            //        wsRes.CallAPIStatusText = "บันทึกข้อมูลสำเร็จ , มีเอกสารบางรายการบันทึกไม่สำเร็จ กรุณาอัพโหลดใหม่อีกครั้ง";
            //        wsRes.Result = answer;
            //    }
            //}

            // Add percentage to Answer
            // 1. check choice and file for calculte percentage
            var subAnswer = await _context.Answer.Where(sa => sa.MainQuestionId == submitReq.MainQuestionId)
                                                        .Where(sa => sa.SubQuestionId == submitReq.SubQuestionId)
                                                        .Where(sa => sa.DepartmentId == userId.DepartmentId)
                                                        .OrderByDescending(sa => sa.AnswerSequence)
                                                        .FirstOrDefaultAsync();

            //var subPercent = "17.5";
            //int Color = 1;
            //if(subAnswer != null)
            //{
            //    var choiceNo = subAnswer.ChoiceNo;
            //    //var file = subAnswer.HasFile;
            //    var files = await _context.Files.Where(fs => fs.MainQuesetionId == submitReq.MainQuestionId && fs.IsActive == 1)
            //                             .Where(fs => fs.SubQuestionId == submitReq.SubQuestionId && fs.DepartmentId == userId.DepartmentId)
            //                             .ToListAsync();
            //    var file = 0;
            //    if (files.Count != 0)
            //    {
            //        file = 1;
            //    }

            //    if (file == 0)
            //    {
            //        switch (choiceNo)
            //        {
            //            case 1:
            //                subPercent = "78";
            //                Color = 4;
            //                break;
            //            case 4:
            //                subPercent = "78";
            //                Color = 4;
            //                break;
            //            case 2:
            //                subPercent = "43";
            //                Color = 2;
            //                break;
            //            case 3:
            //                subPercent = "17.5";
            //                break;
            //            default:
            //                subPercent = "17.5";
            //                break;
            //        }
            //    }
            //    else if (file == 1)
            //    {
            //        switch (choiceNo)
            //        {
            //            case 1:
            //                subPercent = "93";
            //                Color = 5;
            //                break;
            //            case 4:
            //                subPercent = "93";
            //                Color = 5;
            //                break;
            //            case 2:
            //                subPercent = "60.5";
            //                Color = 3;
            //                break;
            //            case 3:
            //                subPercent = "17.5";
            //                break;
            //            default:
            //                subPercent = "17.5";
            //                break;
            //        }
            //    }

            //}

            ////2. check has percentage? 
            //var percent = await _context.UserProcess.Where(pc => pc.PercentageFor == 3)
            //                                        .Where(pc => pc.PercentageForId == submitReq.SubQuestionId)
            //                                        .OrderByDescending(pc => pc.ProcessSequence)
            //                                        .FirstOrDefaultAsync();

            //if (percent == null)
            //{
            //    // no percentage then add new

            //    UserProcess userProcess = new UserProcess();
            //    userProcess.UserId = (int)userId.UserId;
            //    userProcess.DepartmentId = userId.DepartmentId;
            //    userProcess.ProcessSequence = 1;
            //    userProcess.Percentage = Convert.ToDouble(subPercent);
            //    userProcess.PercentageFor = 3; // subquestion
            //    userProcess.PercentageForText = "SubQuestion";
            //    userProcess.PercentageForId = submitReq.SubQuestionId;
            //    userProcess.Color = Color; //red
            //    userProcess.ColorText = "red";
            //    userProcess.ProcessingDate = DateTime.Now;

            //    await _context.UserProcess.AddAsync(userProcess);

            //}
            //else
            //{
            //    // update percent
            //    percent.Percentage = Convert.ToDouble(subPercent);
            //    percent.Color = Color;
            //    percent.ProcessingDate = DateTime.Now;
            //    _context.Entry(percent).State = EntityState.Modified;

            //}

            //try
            //{
            //    await _context.SaveChangesAsync();
            //    // add log

            //}
            //catch (Exception ex)
            //{
            //    // add log
            //    Console.WriteLine(ex.ToString());
            //}

            if (await UpdateDepPercentage(subAnswer))
            {
                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "บันทึกข้อมูลสำเร็จ";
                wsRes.Result = answer;
            }
            else
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "บันทึกข้อมูลไม่สำเร็จ";
                wsRes.Result = answer;
            }

            return Ok(wsRes);
        }

        [HttpPost("SaveFile")]
        [Authorize]
        public async Task<IActionResult> SaveFile(FileUploadReq fileUploadReq)
        {
            var userId = await _context.Users.Where(u => u.Token == fileUploadReq.Token).FirstOrDefaultAsync();

            var files = await _context.Files.Where(f => f.DepartmentId == userId.DepartmentId)
                                            .Where(f => f.MainQuesetionId == fileUploadReq.MainQuestionId)
                                            .Where(f => f.SubQuestionId == fileUploadReq.SubQuestionId)
                                            .Where(f => f.IsActive == 1)
                                            .OrderByDescending(f => f.FileSequence)
                                            .ToListAsync();
            var fileSeq = 0;
            if (files.Count != 0)
            {
                fileSeq = files.FirstOrDefault().FileSequence + 1;
            }

            var subAnswer = await _context.Answer.Where(sa => sa.MainQuestionId == fileUploadReq.MainQuestionId)
                                                        .Where(sa => sa.SubQuestionId == fileUploadReq.SubQuestionId)
                                                        .Where(sa => sa.DepartmentId == userId.DepartmentId)
                                                        .OrderByDescending(sa => sa.AnswerSequence)
                                                        .FirstOrDefaultAsync();

            try
            {
                //for (int f = 0; f < fileUploadReq.Count; f++)
                //{
                Files saveFile = new Files();
                saveFile.UserId = (int)userId.UserId;
                saveFile.DepartmentId = userId.DepartmentId;
                saveFile.MainQuesetionId = (int)fileUploadReq.MainQuestionId;
                saveFile.SubQuestionId = (int)fileUploadReq.SubQuestionId;
                saveFile.IsActive = 1;
                saveFile.FileSequence = fileSeq;
                saveFile.FileName = fileUploadReq.FileName;
                saveFile.FileType = fileUploadReq.FileType;
                saveFile.Base64Str = fileUploadReq.Base64Str;
                saveFile.FilePath = "";
                saveFile.UploadDate = DateTime.Now;

                await _context.Files.AddAsync(saveFile);
                await _context.SaveChangesAsync();
                //}

                // Calculate new percentage

                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "บันทึกไฟล์ "+ fileUploadReq.FileName +"."+ fileUploadReq.FileType +" สำเร็จ";

                await UpdateDepPercentage(subAnswer);
               
                return Ok(wsRes);
            }
            catch (Exception ex)
            {
                // Log
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "บันทึกไฟล์ " + fileUploadReq.FileName + "." + fileUploadReq.FileType + " ล้มเหลว";
                wsRes.Result = new {message =  ex.Message};
                return Ok(wsRes);
            }            
        }

        [HttpPost("DeleteFile")]
        [Authorize]
        public async Task<IActionResult> DeleteFiles(FileDeleteReq fileDeleteReq)
        {
            var userId = await _context.Users.Where(u => u.Token == fileDeleteReq.Token).FirstOrDefaultAsync();

            var files = await _context.Files.Where(f => f.Id == fileDeleteReq.FileId)
                                            .FirstOrDefaultAsync();
            if (files == null)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ไม่พบไฟล์ที่ต้องการลบ";

                return Ok(wsRes);
            }

            files.IsActive = 0;
            _context.Entry(files).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "ลบไฟล์สำเร็จ";
                return Ok(wsRes);
            }
            catch (Exception ex)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "เกิดข้อผิดพลาด";
                wsRes.Result = new {message = ex.Message};

                return Ok(wsRes);
            }

        }

        private async Task<bool> UpdateDepPercentage(Answer subAnswer)
        {
            var subPercent = "17.5";
            int Color = 1;
            if (subAnswer != null)
            {
                var choiceNo = subAnswer.ChoiceNo;
                //var file = subAnswer.HasFile;
                var files = await _context.Files.Where(fs => fs.MainQuesetionId == subAnswer.MainQuestionId && fs.IsActive == 1)
                                         .Where(fs => fs.SubQuestionId == subAnswer.SubQuestionId && fs.DepartmentId == subAnswer.DepartmentId)
                                         .ToListAsync();
                var file = 0;
                if (files.Count != 0)
                {
                    file = 1;
                }

                if (file == 0)
                {
                    switch (choiceNo)
                    {
                        case 1:
                            subPercent = "78";
                            Color = 4;
                            break;
                        case 4:
                            subPercent = "78";
                            Color = 4;
                            break;
                        case 2:
                            subPercent = "43";
                            Color = 2;
                            break;
                        case 3:
                            subPercent = "17.5";
                            break;
                        default:
                            subPercent = "17.5";
                            break;
                    }
                }
                else if (file == 1)
                {
                    switch (choiceNo)
                    {
                        case 1:
                            subPercent = "93";
                            Color = 5;
                            break;
                        case 4:
                            subPercent = "93";
                            Color = 5;
                            break;
                        case 2:
                            subPercent = "60.5";
                            Color = 3;
                            break;
                        case 3:
                            subPercent = "17.5";
                            break;
                        default:
                            subPercent = "17.5";
                            break;
                    }
                }

            }

            //2. check has percentage? 
            var percent = await _context.UserProcess.Where(pc => pc.PercentageFor == 3)
                                                    .Where(pc => pc.PercentageForId == subAnswer.SubQuestionId && pc.DepartmentId == subAnswer.DepartmentId)
                                                    .OrderByDescending(pc => pc.ProcessSequence)
                                                    .FirstOrDefaultAsync();

            if (percent == null)
            {
                // no percentage then add new

                UserProcess userProcess = new UserProcess();
                userProcess.UserId = (int)subAnswer.UserId;
                userProcess.DepartmentId = subAnswer.DepartmentId;
                userProcess.ProcessSequence = 1;
                userProcess.Percentage = Convert.ToDouble(subPercent);
                userProcess.PercentageFor = 3; // subquestion
                userProcess.PercentageForText = "SubQuestion";
                userProcess.PercentageForId = subAnswer.SubQuestionId;
                userProcess.Color = Color; //red
                userProcess.ColorText = "red";
                userProcess.ProcessingDate = DateTime.Now;

                await _context.UserProcess.AddAsync(userProcess);

            }
            else
            {
                // update percent
                percent.Percentage = Convert.ToDouble(subPercent);
                percent.Color = Color;
                percent.ProcessingDate = DateTime.Now;
                _context.Entry(percent).State = EntityState.Modified;

            }

            try
            {
                await _context.SaveChangesAsync();
                // add log

                return true;

            }
            catch (Exception ex)
            {
                // add log
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
    }
}
