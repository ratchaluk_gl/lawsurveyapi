﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using WSLawSurvey.WSFunction;

namespace WSLawSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly APIContext _context;
        private readonly WSRespons wsRes = new WSRespons();

        public UsersController(APIContext context)
        {
            _context = context;
        }

        [HttpPost("GetProfile")]
        public async Task<ActionResult> GetProfile(Token token)
        {
            if (!await WSUtils.CheckToken(token.token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token Expired.";
                wsRes.Result = null;
                return Ok(wsRes);
            }

            var getProfile = await _context.Users.Where(u => u.Token == token.token).FirstOrDefaultAsync();
            if (getProfile == null)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "No User Found.";
                wsRes.Result = null;
                return Ok(wsRes);
            }

            UserProfile userProfile = new UserProfile();
            userProfile.UserId = (int)getProfile.UserId;
            userProfile.Prefix = getProfile.Prefix;
            userProfile.FirstName = getProfile.FirstName;
            userProfile.LastName = getProfile.LastName;
            userProfile.DepartmentId = getProfile.DepartmentId.ToString();
            userProfile.DepartmentName = getProfile.Section;
            userProfile.Section = "";
            userProfile.UserTypeId = (int)getProfile.UserTypeId;
            userProfile.UserTypeText = getProfile.UserTypeId == 0 ? "admin" : "user";

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "Get Profile Success.";
            wsRes.Result = userProfile;

            return Ok(wsRes);
        }
    }
}
