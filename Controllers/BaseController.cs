﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using WSLawSurvey.Areas.Models;
//using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using WSLawSurvey.WSFunction;

namespace WSLawSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly APIContext _context;

        public BaseController(APIContext context)
        {
            _context = context;
        }

        [HttpPost("AddUser")]
        public async Task<ActionResult> AddUser(Users userProfile)
        {
            // Check existing user name
            var cUserName = await _context.Users.Where(u => u.UserName == userProfile.UserName).ToListAsync();
            if (cUserName.Count > 0)
            {
                return Conflict(new { message = $"This User name is already had. => {cUserName[0].UserName}" });
            }

            await _context.Users.AddAsync(userProfile);
            await _context.SaveChangesAsync();


            return Created("", new { message = $"User {userProfile.UserName} Added." });
        }

        [HttpPost("AddDepartment")]
        public async Task<ActionResult<Department>> AddDepartment(Department department)
        {
            await _context.Department.AddAsync(department);
            await _context.SaveChangesAsync();

            //var dep = await _context.Department.FirstAsync();

            return CreatedAtAction("AddDepartment", new {id = department.DepartmentId } , new {message = "Add Data Success" , data = department});
        }

        

    }
}
