﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NuGet.Protocol;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
//using WSLawSurvey.Areas.Identity.Data;
using WSLawSurvey.Areas.Models;
using WSLawSurvey.Data;
using WSLawSurvey.Models;
using WSLawSurvey.WSFunction;

namespace WSLawSurvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        //private readonly UserManager<Users> _userManager;
        //private readonly SignInManager<Users> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly APIContext _context;
        private readonly WSRespons wsRes = new WSRespons();

        public AuthController(
            APIContext apiContext,
            IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _context = apiContext;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginData loginData)
        {
            var user = await _context.Users.Where(u => u.UserName == loginData.UserName).ToListAsync();
            if (user.Count == 0)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ผู้ใช้ รหัส : " + loginData.UserName + " ไม่มีสิทธิเข้าระบบแบบสอบทาน";
                return Ok(wsRes);
            }
            ////Call Intra Login Service
            ////if result = true then create token
            IntraSignInObjReq signInObjReq = new IntraSignInObjReq();
            signInObjReq.Client_key = _configuration.GetValue<string>("Intra:ClientKey");
            signInObjReq.Username = loginData.UserName;
            signInObjReq.Password = loginData.Password;
            signInObjReq.Ip = "10.3.64.108";

            IntraLoginRes LoginRes = new IntraLoginRes();
            LoginRes = await WSUtils.IntraSignIn(signInObjReq);

            if (LoginRes.result != "success")
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = LoginRes.description;
                wsRes.Result = null;
                return Ok(wsRes);
            }

            return await CreateToken(user.FirstOrDefault().UserName, LoginRes.data);
            //return await CreateToken(user.FirstOrDefault().UserName, "");
        }

        private async Task<IActionResult> CreateToken(string? userName , IntraSignInObjRes intra)
        {
            var user = await _context.Users.Where(u => u.UserName == userName).ToListAsync();
            if (user.Count == 0)
            {
                return Unauthorized();
            }

            //Prepaire Token
            var jwt_key = Encoding.UTF8.GetBytes(_configuration.GetSection("JWTKey").Value!);

            //Playload
            var playload = new List<Claim>
            {
                new Claim("userName",userName.ToString()),
                new Claim(JwtRegisteredClaimNames.NameId,user[0].UserName.ToString())
            };

            var tokendescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(playload),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(jwt_key), SecurityAlgorithms.HmacSha256Signature),
                Expires = DateTime.UtcNow.AddDays(1)
            };

            //Create Token
            var tokenhandler = new JwtSecurityTokenHandler();
            var token = tokenhandler.CreateToken(tokendescriptor);

            //Add Token to DB
            var userProfile = await _context.Users.Where(u => u.UserName == userName).ToListAsync();
            var userForToken = userProfile.FirstOrDefault();
            if (userForToken == null)
            {
                wsRes.CallAPIStatus = true;
                wsRes.CallAPIStatusText = "ผู้ใช้ รหัส : " + user.FirstOrDefault().UserName + " ไม่พบ Token";
                return Ok();
            }

            var resultToken = new
            {
                intraToken = intra.Token,
                accessToken = tokenhandler.WriteToken(token),
                expireation = token.ValidTo,
                userFullName = intra.First_name_th + " " + intra.Last_name_th,
                userPosition = intra.Pos_abbr,
                userSection = intra.Dep_abbr,
                userTypeId = userForToken.UserTypeId,
                roles = userForToken.UserTypeId == 0 ? "admin" : "user"
            };
            string? phoneNo = userForToken.PhoneNo;

            if (intra.Contact.Count != 0)
            {
                for (var i = 0; i < intra.Contact.Count; i++)
                {
                    if (intra.Contact[i].Contact_type == "M")
                    {
                        phoneNo = intra.Contact[i].Contact_value;
                    }
                }
            }
           
            userForToken.Token = resultToken.accessToken;
            userForToken.TokenExpire = resultToken.expireation;
            userForToken.FirstName = intra.First_name_th;
            userForToken.LastName = intra.Last_name_th;
            userForToken.Prefix = intra.Title_th;
            userForToken.Section = intra.Dep_abbr;
            userForToken.Position = intra.Pos_abbr;
            userForToken.Email = intra.Email;
            userForToken.PhoneNo = phoneNo;
            userForToken.IsActive = 1;

            //var userRoles = userForToken.UserTypeId == 0 ? "admin" : "user";

            _context.Entry(userForToken).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ผู้ใช้ รหัส : " + user.FirstOrDefault().UserId.ToString() + " ไม่สามารถบันทึก Token ได้";
                wsRes.Result = new {Error =  ex.ToString()};
                return Ok(wsRes);
            }

            //await _context.Users.AddAsync(userForToken);
            LoginHistory history = new LoginHistory();
            history.IpAddress = "127.0.0.1";
            history.UserId = (int)user.FirstOrDefault().UserId;
            history.Token = resultToken.accessToken.ToString();
            history.TokenExpire = resultToken.expireation;
            history.LastLogin = DateTime.Now;

            await _context.LoginHistory.AddAsync(history);
            await _context.SaveChangesAsync();

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "ผู้ใช้ รหัส : " + user.FirstOrDefault().UserName + " เข้าสู่ระบบสำเร็จ";
            wsRes.Result = resultToken;

            return Ok(wsRes);
        }

        [HttpPost("CheckToken")]
        public async Task<ActionResult> CheckToken(CheckTokenReq tokenReq)
        {
            if (!await WSUtils.CheckToken(tokenReq.Token, _context))
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Token หมดอายุ";
                wsRes.Result = null;

                return Unauthorized(wsRes);
            }
            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "ต่ออายุ Token แล้ว";
            wsRes.Result = null;

            return Ok(wsRes);
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> Logout(Token token)
        {
            var user = await _context.Users.Where(u => u.Token == token.token).FirstOrDefaultAsync();
            if (user == null)
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "ไม่พบ Token สำหรับ Logout";
                wsRes.Result = null;
                return Unauthorized(wsRes);
            }

            user.TokenExpire = null;
            user.Token = "";

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex) 
            {
                wsRes.CallAPIStatus = false;
                wsRes.CallAPIStatusText = "Logout ไม่สำเร็จ";
                wsRes.Result = new {Error = ex.ToString()};
            }

            wsRes.CallAPIStatus = true;
            wsRes.CallAPIStatusText = "Logout สำเร็จ";
            wsRes.Result = null;

            return Ok(wsRes);
        }
    }
}
