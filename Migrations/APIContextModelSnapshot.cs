﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WSLawSurvey.Data;

#nullable disable

namespace WSLawSurvey.Migrations
{
    [DbContext(typeof(APIContext))]
    partial class APIContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.15")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("WSLawSurvey.Models.Answer", b =>
                {
                    b.Property<int>("AnswerId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("AnswerId"));

                    b.Property<DateTime?>("AnswerDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("AnswerSequence")
                        .HasColumnType("int");

                    b.Property<int>("AnswerStatus")
                        .HasColumnType("int");

                    b.Property<string>("AnswerStatusText")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("ApprovedBy")
                        .HasColumnType("int");

                    b.Property<int>("ChoiceId")
                        .HasColumnType("int");

                    b.Property<int>("ChoiceNo")
                        .HasColumnType("int");

                    b.Property<string>("DepartmentId")
                        .HasColumnType("varchar(10)");

                    b.Property<int>("HasFile")
                        .HasColumnType("int");

                    b.Property<int>("MainQuestionId")
                        .HasColumnType("int");

                    b.Property<string>("OtherText")
                        .HasColumnType("varchar(max)");

                    b.Property<int>("SubQuestionId")
                        .HasColumnType("int");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("AnswerId");

                    b.ToTable("tb_answer");
                });

            modelBuilder.Entity("WSLawSurvey.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("DepartmentId")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("DepartmentNameEN")
                        .HasColumnType("varchar(200)");

                    b.Property<string>("DepartmentNameTH")
                        .HasColumnType("varchar(200)");

                    b.Property<string>("HierachyCode")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("InitialName")
                        .HasColumnType("varchar(50)");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_department");
                });

            modelBuilder.Entity("WSLawSurvey.Models.Files", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<byte[]>("Base64Str")
                        .HasColumnType("varbinary(max)");

                    b.Property<string>("DepartmentId")
                        .HasColumnType("varchar(10)");

                    b.Property<string>("FileName")
                        .HasColumnType("varchar(200)");

                    b.Property<string>("FilePath")
                        .HasColumnType("varchar(200)");

                    b.Property<int>("FileSequence")
                        .HasColumnType("int");

                    b.Property<string>("FileType")
                        .HasColumnType("varchar(100)");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<int>("MainQuesetionId")
                        .HasColumnType("int");

                    b.Property<int>("SubQuestionId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("UploadDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_files");
                });

            modelBuilder.Entity("WSLawSurvey.Models.LoginHistory", b =>
                {
                    b.Property<int>("HistoryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("HistoryId"));

                    b.Property<string>("Browser")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("IpAddress")
                        .HasColumnType("varchar(20)");

                    b.Property<DateTime?>("LastLogin")
                        .HasColumnType("datetime2");

                    b.Property<string>("Token")
                        .HasColumnType("varchar(max)");

                    b.Property<DateTime?>("TokenExpire")
                        .HasColumnType("datetime2");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("HistoryId");

                    b.ToTable("tb_login_history");
                });

            modelBuilder.Entity("WSLawSurvey.Models.Logs", b =>
                {
                    b.Property<int>("LogId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("LogId"));

                    b.Property<string>("FromFunction")
                        .HasColumnType("varchar(30)");

                    b.Property<string>("FromTable")
                        .HasColumnType("varchar(30)");

                    b.Property<int>("ModifiedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("OldData")
                        .HasColumnType("varchar(max)");

                    b.HasKey("LogId");

                    b.ToTable("tb_logs");
                });

            modelBuilder.Entity("WSLawSurvey.Models.MainQuestion", b =>
                {
                    b.Property<int>("QuestionId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("QuestionId"));

                    b.Property<int>("CreatedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<int>("ModifiedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("QuestionNo")
                        .HasColumnType("int");

                    b.Property<string>("QuestionText")
                        .HasColumnType("varchar(max)");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.HasKey("QuestionId");

                    b.ToTable("tb_main_question");
                });

            modelBuilder.Entity("WSLawSurvey.Models.MainQuestionMemo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("CommentBy")
                        .HasColumnType("varchar(10)");

                    b.Property<DateTime?>("CommentDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CommentSequence")
                        .HasColumnType("int");

                    b.Property<string>("CommentText")
                        .HasColumnType("varchar(max)");

                    b.Property<bool>("IsLast")
                        .HasColumnType("bit");

                    b.Property<int>("MainQuestionId")
                        .HasColumnType("int");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_main_question_memo");
                });

            modelBuilder.Entity("WSLawSurvey.Models.SubQuestion", b =>
                {
                    b.Property<int>("SubQuestionId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("SubQuestionId"));

                    b.Property<int?>("CreatedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<int?>("MainQuestionId")
                        .HasColumnType("int");

                    b.Property<int?>("ModifiedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("SubQuestionNo")
                        .HasColumnType("varchar(5)");

                    b.Property<string>("SubQuestionText")
                        .HasColumnType("varchar(max)");

                    b.HasKey("SubQuestionId");

                    b.ToTable("tb_sub_question");
                });

            modelBuilder.Entity("WSLawSurvey.Models.SubQuestionMemo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("CommentBy")
                        .HasColumnType("varchar(10)");

                    b.Property<DateTime?>("CommentDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CommentSequence")
                        .HasColumnType("int");

                    b.Property<string>("CommentText")
                        .HasColumnType("varchar(max)");

                    b.Property<bool>("IsLast")
                        .HasColumnType("bit");

                    b.Property<int>("MainQuestionId")
                        .HasColumnType("int");

                    b.Property<int>("SubQuestionId")
                        .HasColumnType("int");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_sub_question_memo");
                });

            modelBuilder.Entity("WSLawSurvey.Models.Survey", b =>
                {
                    b.Property<int>("SurveyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("SurveyId"));

                    b.Property<int?>("CreatedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("EndDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Explanations")
                        .HasColumnType("varchar(max)");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<int?>("ModifiedBy")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ModifiedDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("StartDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("SurveyDetail")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("SurveyName")
                        .HasColumnType("varchar(max)");

                    b.HasKey("SurveyId");

                    b.ToTable("tb_surveys");
                });

            modelBuilder.Entity("WSLawSurvey.Models.SurveyMemo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("CommentBy")
                        .HasColumnType("varchar(10)");

                    b.Property<DateTime?>("CommentDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CommentSequence")
                        .HasColumnType("int");

                    b.Property<string>("CommentText")
                        .HasColumnType("varchar(max)");

                    b.Property<bool>("IsLast")
                        .HasColumnType("bit");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_survey_memo");
                });

            modelBuilder.Entity("WSLawSurvey.Models.SurveyPermission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<DateTime?>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("DepartmentId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<int>("SurveyId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("tb_survey_permission");
                });

            modelBuilder.Entity("WSLawSurvey.Models.UserProcess", b =>
                {
                    b.Property<int>("ProcessId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ProcessId"));

                    b.Property<int>("Color")
                        .HasColumnType("int");

                    b.Property<string>("ColorText")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("DepartmentId")
                        .HasColumnType("varchar(10)");

                    b.Property<double?>("Percentage")
                        .HasColumnType("float");

                    b.Property<int>("PercentageFor")
                        .HasColumnType("int");

                    b.Property<int>("PercentageForId")
                        .HasColumnType("int");

                    b.Property<string>("PercentageForText")
                        .HasColumnType("varchar(50)");

                    b.Property<int>("ProcessSequence")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ProcessingDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("ProcessId");

                    b.ToTable("tb_user_process");
                });

            modelBuilder.Entity("WSLawSurvey.Models.UserType", b =>
                {
                    b.Property<int>("UserTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("UserTypeId"));

                    b.Property<int>("IsActive")
                        .HasColumnType("int");

                    b.Property<string>("UserTypeName")
                        .HasColumnType("varchar(50)");

                    b.HasKey("UserTypeId");

                    b.ToTable("tb_user_types");
                });

            modelBuilder.Entity("WSLawSurvey.Models.Users", b =>
                {
                    b.Property<int?>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int?>("UserId"));

                    b.Property<string>("DepartmentId")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Email")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("FirstName")
                        .HasColumnType("varchar(50)");

                    b.Property<int?>("IsActive")
                        .HasColumnType("int");

                    b.Property<string>("LastName")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("PhoneNo")
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Position")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Prefix")
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Section")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Token")
                        .HasColumnType("varchar(max)");

                    b.Property<DateTime?>("TokenExpire")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserName")
                        .HasColumnType("varchar(50)");

                    b.Property<int?>("UserTypeId")
                        .HasColumnType("int");

                    b.HasKey("UserId");

                    b.ToTable("tb_users");
                });
#pragma warning restore 612, 618
        }
    }
}
