﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WSLawSurvey.Migrations
{
    /// <inheritdoc />
    public partial class AlterTableAnswerAndFixWording : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ChooseId",
                table: "tb_answer",
                newName: "ChoiceNo");

            migrationBuilder.AddColumn<int>(
                name: "ChoiceId",
                table: "tb_answer",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChoiceId",
                table: "tb_answer");

            migrationBuilder.RenameColumn(
                name: "ChoiceNo",
                table: "tb_answer",
                newName: "ChooseId");
        }
    }
}
