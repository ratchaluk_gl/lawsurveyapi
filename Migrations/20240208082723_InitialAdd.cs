﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WSLawSurvey.Migrations
{
    /// <inheritdoc />
    public partial class InitialAdd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_answer",
                columns: table => new
                {
                    AnswerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    SurveyId = table.Column<int>(type: "int", nullable: false),
                    MainQuestionId = table.Column<int>(type: "int", nullable: false),
                    SubQuestionId = table.Column<int>(type: "int", nullable: false),
                    ChooseId = table.Column<int>(type: "int", nullable: false),
                    AnswerDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    AnswerSequence = table.Column<int>(type: "int", nullable: false),
                    IsApproved = table.Column<int>(type: "int", nullable: false),
                    ApprovedBy = table.Column<int>(type: "int", nullable: true),
                    IsLastAnswer = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_answer", x => x.AnswerId);
                });

            migrationBuilder.CreateTable(
                name: "tb_choose",
                columns: table => new
                {
                    ChooseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ChooseNo = table.Column<int>(type: "int", nullable: true),
                    ChooseText = table.Column<string>(type: "varchar(20)", nullable: true),
                    ChooseLine = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_choose", x => x.ChooseId);
                });

            migrationBuilder.CreateTable(
                name: "tb_department",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DepartmentName = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_department", x => x.DepartmentId);
                });

            migrationBuilder.CreateTable(
                name: "tb_login_history",
                columns: table => new
                {
                    HistoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Token = table.Column<string>(type: "varchar(max)", nullable: true),
                    TokenExpire = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime", nullable: true),
                    IpAddress = table.Column<string>(type: "varchar(20)", nullable: true),
                    Browser = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_login_history", x => x.HistoryId);
                });

            migrationBuilder.CreateTable(
                name: "tb_logs",
                columns: table => new
                {
                    LogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FromTable = table.Column<string>(type: "varchar(30)", nullable: true),
                    FromFunction = table.Column<string>(type: "varchar(30)", nullable: true),
                    OldData = table.Column<string>(type: "varchar(max)", nullable: true),
                    ModifiedBy = table.Column<int>(type: "int", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_logs", x => x.LogId);
                });

            migrationBuilder.CreateTable(
                name: "tb_main_question",
                columns: table => new
                {
                    QuestionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SurveyId = table.Column<int>(type: "int", nullable: false),
                    QuestionNo = table.Column<int>(type: "int", nullable: false),
                    QuestionText = table.Column<string>(type: "varchar(max)", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<int>(type: "int", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_main_question", x => x.QuestionId);
                });

            migrationBuilder.CreateTable(
                name: "tb_sub_question",
                columns: table => new
                {
                    SubQuestionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MainQuestionId = table.Column<int>(type: "int", nullable: true),
                    SubQuestionText = table.Column<string>(type: "varchar(max)", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<int>(type: "int", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_sub_question", x => x.SubQuestionId);
                });

            migrationBuilder.CreateTable(
                name: "tb_surveys",
                columns: table => new
                {
                    SurveyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SurveyName = table.Column<string>(type: "varchar(max)", nullable: true),
                    SurveyDetail = table.Column<string>(type: "varchar(max)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<int>(type: "int", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_surveys", x => x.SurveyId);
                });

            migrationBuilder.CreateTable(
                name: "tb_user_process",
                columns: table => new
                {
                    ProcessId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    DepartmentId = table.Column<int>(type: "int", nullable: false),
                    SurveyId = table.Column<int>(type: "int", nullable: false),
                    SurveyPercentage = table.Column<double>(type: "float", nullable: true),
                    MainQuestionId = table.Column<int>(type: "int", nullable: false),
                    MainQuestionPercentage = table.Column<double>(type: "float", nullable: true),
                    SubQuestionId = table.Column<int>(type: "int", nullable: false),
                    SubQuestionPercentage = table.Column<double>(type: "float", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_user_process", x => x.ProcessId);
                });

            migrationBuilder.CreateTable(
                name: "tb_user_types",
                columns: table => new
                {
                    UserTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserTypeName = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_user_types", x => x.UserTypeId);
                });

            migrationBuilder.CreateTable(
                name: "tb_users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "varchar(50)", nullable: true),
                    Prefix = table.Column<string>(type: "varchar(20)", nullable: true),
                    FirstName = table.Column<string>(type: "varchar(50)", nullable: true),
                    LastName = table.Column<string>(type: "varchar(50)", nullable: true),
                    DepartmentId = table.Column<int>(type: "int", nullable: true),
                    Section = table.Column<string>(type: "varchar(50)", nullable: true),
                    PhoneNo = table.Column<string>(type: "varchar(10)", nullable: true),
                    UserTypeId = table.Column<int>(type: "int", nullable: true),
                    Token = table.Column<string>(type: "varchar(max)", nullable: true),
                    TokenExpire = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_users", x => x.UserId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_answer");

            migrationBuilder.DropTable(
                name: "tb_choose");

            migrationBuilder.DropTable(
                name: "tb_department");

            migrationBuilder.DropTable(
                name: "tb_login_history");

            migrationBuilder.DropTable(
                name: "tb_logs");

            migrationBuilder.DropTable(
                name: "tb_main_question");

            migrationBuilder.DropTable(
                name: "tb_sub_question");

            migrationBuilder.DropTable(
                name: "tb_surveys");

            migrationBuilder.DropTable(
                name: "tb_user_process");

            migrationBuilder.DropTable(
                name: "tb_user_types");

            migrationBuilder.DropTable(
                name: "tb_users");
        }
    }
}
