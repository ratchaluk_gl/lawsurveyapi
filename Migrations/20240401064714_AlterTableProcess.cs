﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WSLawSurvey.Migrations
{
    /// <inheritdoc />
    public partial class AlterTableProcess : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ProcessingDate",
                table: "tb_user_process",
                type: "datetime2",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProcessingDate",
                table: "tb_user_process");
        }
    }
}
