﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WSLawSurvey.Migrations
{
    /// <inheritdoc />
    public partial class AddColumnAnswerTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "tb_answer",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HasFile",
                table: "tb_answer",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "tb_answer");

            migrationBuilder.DropColumn(
                name: "HasFile",
                table: "tb_answer");
        }
    }
}
