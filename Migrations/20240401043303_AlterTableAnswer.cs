﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WSLawSurvey.Migrations
{
    /// <inheritdoc />
    public partial class AlterTableAnswer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DepartmentId",
                table: "tb_answer",
                type: "varchar(10)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "tb_answer");
        }
    }
}
