﻿using System.ComponentModel.DataAnnotations;

namespace WSLawSurvey.Areas.Models
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set;}
        public string? Prefix { get; set; }
        public string? DepartmentId { get; set; }
        public string? DepartmentName { get; set; }
        public string? Section {  get; set; }
        public int UserTypeId { get; set; }
        public string? UserTypeText { get; set; }
    }
    //public class Department
    //{
    //    public int DepartmentId { get; set; }
    //    public string? DepartmentName { get; set; }
    //    public int IsActive { get; set; }
    //}
    public class LoginData
    {
        [Required(ErrorMessage ="This field is require.")]
        public string? UserName { get; set; }
        [Required(ErrorMessage ="Password can't be Empty.")]
        //[StringLength(8)]
        public string? Password { get; set; }
        public string? IpAddress {  get; set; }
    }
    public class Token
    {
        public string? token { get; set;}
    }

    // Intra
    public class IntraSignInObjReq
    {
        public string? Client_key { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Ip { get; set; }
    }

    public class IntraSignInObjRes
    {
        public string? Token { get; set; }
        public string? Empcode {  get; set; }
        public string? Title_th {  get; set; }
        public string? First_name_th {  get; set; }
        public string? Last_name_th { get; set;}
        public string? Title_en { get; set; }
        public string? First_name_en { get; set; }
        public string? Last_name_en { get; set; }
        public string? Nick_name { get; set; }
        public string? Gender {  get; set; }
        public string? Pos_abbr {  get; set; }
        public string? Pos_level {  get; set; }
        public string? Pos_full {  get; set; }
        public string? Dep_abbr {  get; set; }
        public string? Dep_full {  get; set; }
        public string? Username { get; set;}
        public string? Email {  get; set; }
        public string? Employee_type {  get; set; }
        public bool? Is_os { get; set; }
        public string? Image_url {  get; set; }
        public List<Contact>? Contact {  get; set; }
    }
    public class Contact
    {
        public string? Contact_type { get; set; }
        public string? Contact_value {  get; set; }

    }

    public class IntraLoginRes
    {
        public string? result { get; set; }
        public string? description { get; set; }
        public int? rows { get; set; }
        public IntraSignInObjRes? data { get; set; } = null;
    }
}
