﻿using System.ComponentModel.DataAnnotations;
using WSLawSurvey.Models;

namespace WSLawSurvey.Areas.Models
{
    // Users
    public class CheckTokenReq
    {
        public string? Token { get; set; }
    }

    public class GetSurveyReq
    {
        public string? Token { get; set; }
        public int? SurveyId { get; set; }
    }

    public class GetSubQuestionByIdReq
    {
        public string Token { get; set; }
        public int MainQuestionId { get; set; }
        public int SubQuestionId { get; set; }
    }

    public class GetDepartmentSubQuestionByIdReq
    {
        public string Token { get; set; }
        public string? DepartmentId { get; set; }
        public int MainQuestionId { get; set; }
        public int SubQuestionId { get; set; }
    }

    public class SubmitQuestionReq
    {
        public string? Token { get; set; }
        public int SurveyId { get; set; }
        public int MainQuestionId { get; set; }
        public int SubQuestionId { get; set;}
        public int ChoiceNo { get; set; }
        public string? OtherText { get; set; } = "";
        //public List<FileUploadReq>? Files { get; set; }

    }
    public class FileUploadReq
    {
        public string? Token { get; set; }
        public int? MainQuestionId { get; set; }
        public int? SubQuestionId { get; set; }
        public string? FileName { get; set; }
        public string? FileType { get; set; }
        public string? Base64Str {  get; set; }
    }

    public class FileDeleteReq
    {
        public string? Token { get; set; }
        public int FileId { get; set; }
    }

    public class SubQuestionProcessHistoryReq
    {
        public string? Token { get; set; }
        public int SubQuestionId { get; set; }
    }

    public class AddLogReq
    {
        public string? FromTable { get; set; }
        public string? FromFunction { get; set; }
        public string? OldData { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int ModifiedBy {  get; set; }
    }

    // Admin
    public class GetAllDepartmentReq
    {
        public string? Token { get; set;}
        public int? SurveyId { get; set; }
    }

    public class  GetDepartmentSurveyReq
    {
        public string? Token { get; set; }
        public int? SurveyId { get; set; }
        public string? DepartmentId {  get; set; }
    }

    public class AddNewSurveyReq
    {
        [Required(ErrorMessage ="Token is Required.")]
        public string? Token { get; set; }
        [Required(ErrorMessage = "กรุณากรอกชื่อแบบสอบทาน")]
        public string? SurveyName {  get; set; }
        public string? SurveyDetail {  get; set; }
        [Required(ErrorMessage = "กรุณากรอกคำอธิบายแบบสอบทาน")]
        public string? Explanations {  get; set; }
        [Required(ErrorMessage = "กรุณาเพิ่มวันที่เริ่มต้นแบบสอบทาน")]
        public DateTime? StartDate {  get; set; }
        [Required(ErrorMessage = "กรุณาเพิ่มวันที่สิ้นสุดแบบสอบทาน")]
        public DateTime? EndDate { get; set;}
    }

    public class ResponsdentsAssignmentReq
    {
        [Required(ErrorMessage = "Token is Required.")]
        public string? Token { get; set; }
        [Required(ErrorMessage = "Username is Required.")]
        public string? UserName { get; set; }
        [Required(ErrorMessage = "Firstname is Required.")]
        public string? FirstName { get; set; }
        [Required(ErrorMessage = "Lastname is Required.")]
        public string? LastName { get; set;}
        [Required(ErrorMessage = "Department is Required.")]
        public string? DepartmentId { get; set; }
        [MaxLength(10)]
        public string? PhoneNo { get; set; }
        [Required(ErrorMessage = "UserType is Required.")]
        public int? UserTypeId { get; set; }
        [Required(ErrorMessage = "SurveyId is Required.")]
        public int? SurveyId { get; set; }
    }

    public class AddCommentToSubQuestionReq
    {
        public string? Token { get; set; }
        public int SurveyId { get; set; }
        public int MainQuestionId { get; set; }
        public int SubQuestionId {  get; set; }
        public string? CommentText { get; set; }
    }

    public class AddMainQuestionReq
    {
        public string? Token { get; set; }
        public int SurveyId { get; set; }
        public int MainQuestionNo {  get; set; }
        public string? MainQuestionText {  get; set; }
    }
}
