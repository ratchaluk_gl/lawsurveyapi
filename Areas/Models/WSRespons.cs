﻿namespace WSLawSurvey.Areas.Models
{
    // Users
    public class WSRespons
    {
        public bool CallAPIStatus { get; set; }
        public string? CallAPIStatusText { get; set; }
        public object? Result { get; set; } = null;
    }

    public class ResponseUsersSurvey
    {
        public int SurveyId { get; set; }
        public string? SurveyName { get;set; }
        public string? SurveyDetail { get; set; }
        public DateTime? StartDate {  get; set; }
        public DateTime? EndDate { get; set;}
        public int? IsActive {  get; set; }
    }

    public class ResponseMainQuestion
    {
        public int SurveyId { get; set; }
        public int? UserId { get; set; }
        public string? DepartmentId {  get; set; }
        public int QuestionId { get; set; }
        public string? QuestionNo { get; set; }
        public string? QuestionText { get; set; }
        public Double? MainProcessPercentage { get; set; }
        public int MainQuestionColor { get; set; } = 0;
        public List<ResponseSubQuestion>? SubQuestions { get; set; }
    }

    public class ResponseSubQuestion
    {
        public int SubQuestionId { get; set;}
        public int SurveyId {  get; set; }
        public int MainQuestionId {  get; set; }
        public string? MainQuestionText {  get; set; }
        public string? SubQuestionNo { get; set;}
        public string? SubQuestionText { get; set;}
        //public int Choice1 { get; set;} = 0;
        //public int Choice2 { get; set; } = 0;
        //public int Choice3 { get; set; } = 0;
        public int ChoiceNo { get; set; } = 0;
        public string? OtherText {  get; set; }
        public int HasFiles { get; set; } = 0;
        public List<ResponseFiles>? Files { get; set; } = null;
        public string? CommentFromAdmin {  get; set; }
        public Double? SubProcessPercentage {  get; set; }
        public int SubQuestionStatus { get; set; } = 0;
        public string? SubQuestionStatusText { get; set; }
        public int SubQuestionColor { get; set; } = 0;
    }

    public class ResponseFiles
    {
        public int FileId {  get; set; }
        public string? FileName { get; set; }
        public string? FileType {  get; set; }
        public string? Base64Str { get; set; }
    }

    public class ResSubQuestionProcessHistory
    {
        public int UserId { get; set; }
        public int SurveyId {  get; set; }
        public int MainQuestionId {  get; set; }
        public int SubQuestionId { get; set; }
        public string? Percentage {  get; set; }
        public DateTime? AnswerDate {  get; set; }
        public string? CommentText { get; set; }
        public DateTime? CommentDate {  get; set; }
        public int Sequence {  get; set; }
    }

    // Admin
    public class ResAllDepartmentStat
    {
        public int TotalCount { get; set; }
        public int ActionAnswerCount {  get; set; }
        public int NoActionAnswerCount {  get; set; }
        public List<ResDepartmentListWithProcess>? DepartmentList {  get; set; }
    }
    public class ResDepartmentListWithProcess
    {
        public string? DepartmentId { get; set; }
        public string? DepartmentName { get; set; }
        public string? DepartmentInitName { get; set; }
        public int SectionId { get; set; }
        public string? SectionName { get; set; }
        public string? Percentage { get; set; }
        public int Color {  get; set; }
        public int DepartmentStatusId {  get; set; }
        public string? PrePercentage { get; set; }
        public string? PostPercentage { get; set; }
    }
}
