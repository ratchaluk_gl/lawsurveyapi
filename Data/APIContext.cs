﻿using Microsoft.EntityFrameworkCore;
using WSLawSurvey.Models;

namespace WSLawSurvey.Data
{
    public class APIContext : DbContext
    {
        public APIContext(DbContextOptions<APIContext> options)
            : base(options)
        { }
        public DbSet<WSLawSurvey.Models.Survey>? Survey { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.Department>? Department { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.MainQuestion>? MainQuestion { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.SubQuestion>? SubQuestion { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.Users>? Users { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.UserType>? UserType { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.UserProcess>? UserProcess { get; set; } = null!;
        //public DbSet<WSLawSurvey.Models.Choice>? Choice { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.Answer>? Answer { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.Logs>? Logs { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.LoginHistory>? LoginHistory { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.SurveyMemo>? SurveyMemo { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.MainQuestionMemo>? MainQuestionMemo { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.SubQuestionMemo>? SubQuestionMemo { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.Files>? Files { get; set; } = null!;
        public DbSet<WSLawSurvey.Models.SurveyPermission>? SurveyPermission { get; set; } = null!;
    }
}
