﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_department")]
    public class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? DepartmentId { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? HierachyCode { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? InitialName { get; set; }
        [Column(TypeName ="varchar(200)")]
        public string? DepartmentNameTH { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string? DepartmentNameEN { get; set; }
        public int IsActive { get; set; } = 1;

    }
}
