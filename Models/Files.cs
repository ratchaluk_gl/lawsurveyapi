﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_files")]
    public class Files
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string? DepartmentId { get; set; }
        public int SubQuestionId { get; set; }
        public int MainQuesetionId { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string? FileName { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string? FilePath { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string? FileType { get; set; }
        [Column(TypeName = "varbinary(max)")]
        public string? Base64Str { get; set; }
        public int FileSequence { get; set; }
        public DateTime? UploadDate { get; set; }
        public int IsActive {  get; set; }

    }
}
