﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_answer")]
    public class Answer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AnswerId { get; set; }
        public int UserId { get; set; }
        [Column(TypeName ="varchar(10)")]
        public string? DepartmentId {  get; set; }
        public int SurveyId { get; set; }
        public int MainQuestionId { get; set; }
        public int SubQuestionId { get; set; }
        public int ChoiceId { get; set; }
        public int ChoiceNo { get; set; }
        [Column(TypeName ="varchar(max)")]
        public string? OtherText {  get; set; }
        public int HasFile {  get; set; }
        public DateTime? AnswerDate { get; set; }
        public int AnswerSequence { get; set; }
        public int AnswerStatus { get; set; } = 0;
        public string? AnswerStatusText {  get; set; }
        [ForeignKey("UserId")]
        public int? ApprovedBy { get; set; }
    }
}
