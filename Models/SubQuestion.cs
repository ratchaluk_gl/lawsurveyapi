﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_sub_question")]
    public class SubQuestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubQuestionId { get; set; }
        [ForeignKey("QuestionId")]
        public int? MainQuestionId { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? SubQuestionText { get; set; }
        [Column(TypeName = "varchar(5)")]
        public string? SubQuestionNo { get; set;}
        [ForeignKey("UserId")]
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [ForeignKey("UserId")]
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
