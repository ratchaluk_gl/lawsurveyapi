﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_user_types")]
    public class UserType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserTypeId { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? UserTypeName { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
