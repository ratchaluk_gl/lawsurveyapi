﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_login_history")]
    public class LoginHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; } 
        public int UserId { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? Token { get; set; }
        public DateTime? TokenExpire { get; set; }
        public DateTime? LastLogin { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string? IpAddress { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Browser {  get; set; }
    }
}
