﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_user_process")]
    public class UserProcess
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProcessId { get; set; }
        public int UserId { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string? DepartmentId { get; set; }
        public int ProcessSequence { get; set; }
        public int PercentageFor {  get; set; }
        public DateTime? ProcessingDate { get; set; }
        [Column(TypeName ="varchar(50)")]
        public string? PercentageForText { get; set; }
        public int PercentageForId {  get; set; }
        public Double? Percentage { get;set; }
        public int Color {  get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? ColorText {  get; set; }
    }
}
