﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_main_question_memo")]
    public class MainQuestionMemo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SurveyId { get; set; }
        public int MainQuestionId { get; set; }
        public int UserId { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string? CommentBy { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? CommentText { get; set; }
        public DateTime? CommentDate { get; set; }
        public int CommentSequence { get; set; }
        public bool IsLast { get; set; }
    }
}
