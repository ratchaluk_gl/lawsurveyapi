﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_users")]
    public class Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? UserId { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? UserName { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string? Prefix { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? FirstName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? LastName { get; set; }
        //[ForeignKey("DepartmentId")]
        [Column(TypeName = "varchar(50)")]
        public string? DepartmentId { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Section { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Position { get; set; }
        //[Column(TypeName = "varchar(50)")]
        //public string? DepartmentName { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string? PhoneNo { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Email { get; set; }
        public int? UserTypeId { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? Token { get; set; }
        public DateTime? TokenExpire { get; set; }
        public int? IsActive { get; set; } = 1;
    }
}
