﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_choice")]
    public class Choice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChooseId { get; set; }
        public int? ChooseNo { get; set; }
        [Column(TypeName ="varchar(20)")]
        public string? ChooseText { get; set; }
        public int? ChooseLine { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
