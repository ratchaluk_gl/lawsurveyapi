﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_logs")]
    public class Logs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogId { get; set; }
        [Column(TypeName ="varchar(30)")]
        public string? FromTable { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string? FromFunction { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? OldData { get; set; }
        [ForeignKey("UserId")]
        public int ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
