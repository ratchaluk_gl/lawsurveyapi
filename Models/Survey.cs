﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_surveys")]
    public class Survey
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SurveyId { get; set; }
        [Column(TypeName="varchar(max)")]
        public string? SurveyName { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? SurveyDetail { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? Explanations { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [ForeignKey("UserId")]
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [ForeignKey("UserId")]
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
