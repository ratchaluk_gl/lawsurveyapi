﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_survey_permission")]
    public class SurveyPermission
    {
        [Key][Required][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public string? DepartmentId { get; set; }
        public int IsActive { get; set; }
        public DateTime? CreatedDate {  get; set; }
    }
}
