﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSLawSurvey.Models
{
    [Table("tb_main_question")]
    public class MainQuestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionId { get; set; }
        public int SurveyId { get; set; }
        public int QuestionNo { get; set; }
        [Column(TypeName ="varchar(max)")]
        public string? QuestionText { get; set; }
        [ForeignKey("UserId")]
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [ForeignKey("UserId")]
        public int ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
